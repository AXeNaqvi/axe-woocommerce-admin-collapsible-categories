(function($){
	$(document).ready(function(){
		set_axe_woo_cat_ul_toggler();
		set_axe_woo_cat_toggler();

		function set_axe_woo_cat_ul_toggler(){
			$('ul.product_cat-checklist li, ul#product_catchecklist li').each(function(){
				var t = $(this),
				c = $('ul.children', t);
				if(c.length){
					t.append('<a href="#" class="axe_ul_tag_toggler">+</a>');
					t.addClass('withchildren');
					axe_woo_cat_ul_toggler_hook();
				}
			})
		}
		function axe_woo_cat_ul_toggler_hook(){
			$('a.axe_ul_tag_toggler').off('click').on('click',function(e){
				e.preventDefault();
				e.stopPropagation();

				var t = $(this),
				p = $(t.parent('li')),
				txt = t.text() == '+' ? '-' : '+';

				t.text(txt);
				p.toggleClass('axe_showchildren');
			});
		}
		function set_axe_woo_cat_toggler(){
			var trs = $('#the-list tr'),
			tlen = trs.length;
			if(tlen){
				zerolevels = 0;
				for(var ix = 0; ix < tlen; ix++){
					var ctr = $(trs[ix]),
					lvl = get_axe_woo_cat_level(ctr),
					lvl2 = lvl;
					if(ix+1 < tlen){
						trsib = $(trs[ix + 1]);
						lvl2 = get_axe_woo_cat_level(trsib);
					}
					/*console.log(ix + ': ' +lvl + ' - ' + lvl2);*/
					if(lvl == 0){
						zerolevels++;
						cls = zerolevels % 2 == 1 ? 'odd' : 'even';
						ctr.addClass('axe_zero_'+cls);
					}
					if(lvl < lvl2){
						$('.column-axetoggle a', ctr).addClass('axe_woo_has_child');
					}
				}
				$('#the-list').addClass('axe_toggleready');
			}
			axe_woo_cat_toggler_hook();
		}
		function axe_woo_cat_toggler_hook(){
			$('a.axe_tag_toggler').on('click',function(e){
				e.preventDefault();
				e.stopPropagation();

				var t = $(this),
				txt  = t.text() == '+' ? '-' : '+',
				p = t.parents('tr'),
				lvl = get_axe_woo_cat_level(p),
				lvlplus = lvl + 1,
				nextlvls = $(p.nextUntil('tr.level-' + lvl));
				t.text(txt);
				if(nextlvls.length){
					ix = 0;
					for(var ix = 0; ix < nextlvls.length; ix++){
						var sib = $(nextlvls[ix]);
						if(sib.hasClass('level-'+lvlplus)){
							sib.toggleClass('axewootablerow');
						}
					}
				}
			});
		}
		function get_axe_woo_cat_level(p){
			lvl = p.attr('class').split(' ');
			lvl = lvl[0].split('-');
			lvl = parseInt(lvl[lvl.length - 1]);
			return lvl;
		}
	});
})(jQuery);