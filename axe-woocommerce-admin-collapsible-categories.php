<?php
/**
 * Plugin Name: AXe WooCommerce Admin Collapsible Categories
 * Plugin URI: https://bitbucket.org/AXeNaqvi/axe-woocommerce-admin-collapsible-categories
 * Description: Adds support for WooCommerce Categories in Admin to be viewed in collapsible form.
 * Author: AXe
 * Author URI: https://bitbucket.org/AXeNaqvi
 * Version: 1.0.0
 * WC requires at least: 3.7.1
 * WC tested up to: 3.7.1
 *
 * Copyright: (c) 2019 AXe (axe.naqvi@gmail.com)
 *
 * License: GNU General Public License v2.0 and above
 * License URI: http://www.gnu.org/licenses/gpl-2.0.html
 *
 */

if ( ! defined( 'ABSPATH' )) exit;

define('AXEADMINWOOCOLLAPSIBLECATEGORIES','axe-woocommerce-admin-collapsible-categories');
define('AXEADMINWOOCOLLAPSIBLECATEGORIESVER', '1.0.0');
define('AXEADMINWOOCOLLAPSIBLECATEGORIESBASE', plugin_dir_url( __FILE__ ));

class AXe_WooCommerceAdminCollapsibleCategories{
	function __construct(){
		add_action('admin_init',array($this, 'axe_acc_init'),9999);
	}

	function axe_acc_init(){
		$this->setFilters();
		$this->setActions();
	}
	private function setFilters(){
		add_filter('manage_edit-product_cat_columns', array($this, 'add_column'),99,1);
		add_filter ('manage_product_cat_custom_column', array($this, 'show_column'), 99,3);
	}
	private function setActions(){
		add_action('edit_product_cat_per_page', array($this,'axe_per_page'),100, 1);
		add_action( 'admin_enqueue_scripts', array($this, 'admin_enquescripts') );
	}
	function axe_per_page($perpage){
		$perpage = apply_filters('axe_woo_collapsible_cats_limit',1000);
		return $perpage;
	}

	function add_column($columns){
		$columns['axetoggle'] = '';
		return $columns;
	}
	function show_column( $columns, $column, $id){
		if ($column == 'axetoggle') {
			$columns .= '<a href="#" class="axe_tag_toggler" title="'.__('Click to toggle sub-categories', AXEADMINWOOCOLLAPSIBLECATEGORIES).'">+</a>';
		}
		return $columns;
	}
	function admin_enquescripts(){
		wp_enqueue_script(AXEADMINWOOCOLLAPSIBLECATEGORIES . '-js', AXEADMINWOOCOLLAPSIBLECATEGORIESBASE.'assets/js/script.js', 'jquery', AXEADMINWOOCOLLAPSIBLECATEGORIESVER,true);
		wp_enqueue_style(AXEADMINWOOCOLLAPSIBLECATEGORIES . '-css', AXEADMINWOOCOLLAPSIBLECATEGORIESBASE.'assets/css/style.css', false, AXEADMINWOOCOLLAPSIBLECATEGORIESVER,false);
	}

	private function isValidArray($arr){
		$res = false;
		if(is_array($arr)){
			if(sizeof($arr)){
				$res = true;
			}
		}
		return $res;
	}
}

if (in_array('woocommerce/woocommerce.php', apply_filters('active_plugins', get_option('active_plugins')))) {
	new AXe_WooCommerceAdminCollapsibleCategories();
}